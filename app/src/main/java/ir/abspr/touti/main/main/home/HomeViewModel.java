package ir.abspr.touti.main.main.home;


import io.reactivex.Single;
import ir.abspr.touti.main.main.base.ViewModel;
import ir.abspr.touti.main.main.http.ResponseArray;
import ir.abspr.touti.main.main.model.Pack;
import ir.abspr.touti.main.main.model.repo.PackRepository;

public class HomeViewModel extends ViewModel {

    private PackRepository packRepository;

    public HomeViewModel(PackRepository packRepository) {
        this.packRepository = packRepository;
    }


    public Single<ResponseArray<Pack>> getPacks() {
        return packRepository.packages();
    }
}
