package ir.abspr.touti.main.main.model.db;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import io.reactivex.Single;
import ir.abspr.touti.main.main.model.Word;
import retrofit2.http.GET;

//@Dao
//public interface WordDao {
//
//    @Query("SELECT * FROM tbl_word")
//    Single<List<Word>> getPinedWords();
//
//    @Insert(onConflict = OnConflictStrategy.REPLACE)
//    void pinWord(Word word);
//
//    @Delete
//    void removePinnedWord(Word word);
//
//}
