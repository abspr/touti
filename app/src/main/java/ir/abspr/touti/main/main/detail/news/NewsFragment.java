package ir.abspr.touti.main.main.detail.news;

import android.os.Bundle;
import android.view.View;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import ir.abspr.touti.R;
import ir.abspr.touti.main.main.base.BaseFragment;
import ir.abspr.touti.main.main.model.wordDetails.News;


public class NewsFragment extends BaseFragment {

    private List<News> newsList;
    private NewsAdapter newsAdapter;


    public NewsFragment(List<News> newsList) {
        this.newsList = newsList;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_news;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupViews(view);
    }


    private void setupViews(View view) {
        RecyclerView recyclerView = view.findViewById(R.id.news_rv);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        newsAdapter = new NewsAdapter(newsList);
        recyclerView.setAdapter(newsAdapter);
    }

}
