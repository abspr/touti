package ir.abspr.touti.main.main.detail.movie;

import android.os.Bundle;
import android.view.View;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import ir.abspr.touti.R;
import ir.abspr.touti.main.main.base.BaseFragment;
import ir.abspr.touti.main.main.model.wordDetails.Movie;

public class MovieFragment extends BaseFragment {

    private List<Movie> movies;

    public MovieFragment(List<Movie> movies) {
        this.movies = movies;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews();
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_movie;
    }

    private void setupViews() {
        RecyclerView recyclerView = getView().findViewById(R.id.movie_rv);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        MovieAdapter movieAdapter = new MovieAdapter(movies);
        recyclerView.setAdapter(movieAdapter);
    }
}
