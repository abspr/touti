package ir.abspr.touti.main.main.detail.definition;

import android.speech.tts.TextToSpeech;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import ir.abspr.touti.R;
import ir.abspr.touti.main.main.RVAdapter;
import ir.abspr.touti.main.main.RVViewHolder;
import ir.abspr.touti.main.main.model.wordDetails.Example;

public class ExampleAdapter extends RVAdapter<Example, ExampleAdapter.ExampleViewHodler> {


    public ExampleAdapter(List<Example> items) {
        super(items);
    }

    @NonNull
    @Override
    public ExampleViewHodler onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_example, parent, false);
        return new ExampleViewHodler(view);
    }

    class ExampleViewHodler extends RVViewHolder<Example> {

        private TextView sentenceTextView;
        private TextView translationTextView;
        TextToSpeech textToSpeech;

        public ExampleViewHodler(@NonNull View itemView) {
            super(itemView);
            sentenceTextView = itemView.findViewById(R.id.example_tv_sentence);
            translationTextView = itemView.findViewById(R.id.example_tv_translation);
            sentenceTextView.setOnClickListener(view -> {
                textToSpeech = new TextToSpeech(itemView.getContext(), i -> {
                    textToSpeech.setLanguage(Locale.US);
                    HashMap<String, String> params = new HashMap<>();
                    params.put(TextToSpeech.Engine.KEY_PARAM_VOLUME, "1");
                    textToSpeech.speak(sentenceTextView.getText().toString(), TextToSpeech.QUEUE_FLUSH, params);
                });
            });
        }

        @Override
        public void bind(Example item) {
            sentenceTextView.setText(item.getSentance());
            translationTextView.setText(item.getTranslation());
        }
    }

}
