package ir.abspr.touti.main.main.model.repo;

import io.reactivex.Single;
import ir.abspr.touti.main.main.http.ApiService;
import ir.abspr.touti.main.main.http.ResponseArray;
import ir.abspr.touti.main.main.model.Section;

public class SectionRepository {

    private ApiService cloudDataSource;

    public SectionRepository(ApiService cloudDataSource) {
        this.cloudDataSource = cloudDataSource;
    }

    public Single<ResponseArray<Section>> sections(int packId) {
        return cloudDataSource.sections(packId);
    }
}
