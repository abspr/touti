package ir.abspr.touti.main.main.detail.music;

import android.os.Bundle;
import android.view.View;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import ir.abspr.touti.R;
import ir.abspr.touti.main.main.base.BaseFragment;
import ir.abspr.touti.main.main.model.wordDetails.Music;

public class MusicFragment extends BaseFragment {

    private List<Music> musicList;

    public MusicFragment(List<Music> musicList) {
        this.musicList = musicList;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews();
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_music;
    }



    private void setupViews() {
        RecyclerView recyclerView = getView().findViewById(R.id.music_rv);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        MusicAdapter musicAdapter = new MusicAdapter(musicList);
        recyclerView.setAdapter(musicAdapter);
    }
}
