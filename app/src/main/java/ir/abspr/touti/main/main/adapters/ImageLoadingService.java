package ir.abspr.touti.main.main.adapters;

import android.widget.ImageView;

public class ImageLoadingService {

    public static void loadImage(ImageView imageView, String path) {
        PicassoImageLoading.loadImage(imageView, path);
    }

}
