package ir.abspr.touti.main.main.base;

import io.reactivex.disposables.CompositeDisposable;

public abstract class ObserverFragment extends BaseFragment {

    protected CompositeDisposable compositeDisposable = new CompositeDisposable();
    protected boolean isSubscribeCalled = false;


    @Override
    public void onStart() {
        super.onStart();
        if (!isSubscribeCalled) {
            subscribe();
            isSubscribeCalled = true;
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        unSubscribe();
    }

    public abstract void subscribe();

    public void unSubscribe() {
        compositeDisposable.clear();
    }

}
