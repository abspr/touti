package ir.abspr.touti.main.main;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public abstract class RVViewHolder<T> extends RecyclerView.ViewHolder {
    public RVViewHolder(@NonNull View itemView) {
        super(itemView);
    }

    public abstract void bind(T item);
}
