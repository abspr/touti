package ir.abspr.touti.main.main.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import ir.abspr.touti.main.main.enums.PackLevel;

@Entity(tableName = "tbl_pack")
public class Pack implements Parcelable {
    @PrimaryKey
    private int id;
    private String title;
    private String cover;
    private int sections;
    private int words;
    private int level;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public int getSections() {
        return sections;
    }

    public void setSections(int sections) {
        this.sections = sections;
    }

    public int getWords() {
        return words;
    }

    public void setWords(int words) {
        this.words = words;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public PackLevel level() {
        return PackLevel.valueOf(level);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.title);
        dest.writeString(this.cover);
        dest.writeInt(this.sections);
        dest.writeInt(this.words);
        dest.writeInt(this.level);
    }

    public Pack() {
    }

    protected Pack(Parcel in) {
        this.id = in.readInt();
        this.title = in.readString();
        this.cover = in.readString();
        this.sections = in.readInt();
        this.words = in.readInt();
        this.level = in.readInt();
    }

    public static final Parcelable.Creator<Pack> CREATOR = new Parcelable.Creator<Pack>() {
        @Override
        public Pack createFromParcel(Parcel source) {
            return new Pack(source);
        }

        @Override
        public Pack[] newArray(int size) {
            return new Pack[size];
        }
    };
}
