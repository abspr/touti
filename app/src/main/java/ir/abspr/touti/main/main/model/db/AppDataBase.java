package ir.abspr.touti.main.main.model.db;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import ir.abspr.touti.main.main.model.Pack;
import ir.abspr.touti.main.main.model.Word;

//@Database(entities = {Pack.class, Word.class}, version = 1, exportSchema = false)
//public abstract class AppDataBase extends RoomDatabase {
//
//    private static AppDataBase appDataBase;
//
//    public static AppDataBase getInstance(Context context) {
//        if (appDataBase == null) {
//            appDataBase = Room.databaseBuilder(context, AppDataBase.class, "db")
//                    .allowMainThreadQueries()
//                    .build();
//        }
//        return appDataBase;
//    }
//
//    public abstract PackDao packDao();
//    public abstract WordDao wordDao();
//}
