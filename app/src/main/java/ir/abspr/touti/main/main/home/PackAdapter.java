package ir.abspr.touti.main.main.home;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import ir.abspr.touti.R;
import ir.abspr.touti.main.main.RVAdapter;
import ir.abspr.touti.main.main.RVViewHolder;
import ir.abspr.touti.main.main.adapters.ImageLoadingService;
import ir.abspr.touti.main.main.model.Pack;

public class PackAdapter extends RVAdapter<Pack, PackAdapter.PackViewHolder> {

    Context context;

    public PackAdapter(List<Pack> items, Context context) {
        super(items);
        this.context = context;
    }

    @NonNull
    @Override
    public PackViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_pack, viewGroup, false);
        return new PackViewHolder(view);
    }



    class PackViewHolder extends RVViewHolder<Pack> {

        TextView titleTextView;
        TextView sectionTextView;
        TextView wordTextView;
        TextView levelTextView;
        ImageView coverImageView;

        public PackViewHolder(@NonNull View itemView) {
            super(itemView);
            titleTextView = itemView.findViewById(R.id.pack_tv_title);
            sectionTextView = itemView.findViewById(R.id.pack_tv_sections);
            wordTextView = itemView.findViewById(R.id.pack_tv_words);
            levelTextView = itemView.findViewById(R.id.pack_tv_level);
            coverImageView = itemView.findViewById(R.id.pack_iv_cover);
        }

        @Override
        public void bind(Pack item) {
            titleTextView.setText(item.getTitle());
            sectionTextView.setText(context.getString(R.string.pack_sections_number, item.getSections()));
            wordTextView.setText(context.getString(R.string.pack_words_number, item.getWords()));
            levelTextView.setText(item.level().description(context));
            ImageLoadingService.loadImage(coverImageView, item.getCover());
        }
    }

}
