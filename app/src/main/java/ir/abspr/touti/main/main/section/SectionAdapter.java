package ir.abspr.touti.main.main.section;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import ir.abspr.touti.R;
import ir.abspr.touti.main.main.RVAdapter;
import ir.abspr.touti.main.main.RVViewHolder;
import ir.abspr.touti.main.main.model.Section;

public class SectionAdapter extends RVAdapter<Section, SectionAdapter.SectionViewHolder> {


    public SectionAdapter(List<Section> items) {
        super(items);
    }

    @NonNull
    @Override
    public SectionViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_section, viewGroup, false);
        return new SectionViewHolder(view);
    }



    public class SectionViewHolder extends RVViewHolder<Section> {

        TextView titleTextView;
        TextView subtitleTextView;

        public SectionViewHolder(@NonNull View itemView) {
            super(itemView);
            titleTextView = itemView.findViewById(R.id.section_tv_title);
            subtitleTextView = itemView.findViewById(R.id.section_tv_subtitle);
        }

        @Override
        public void bind(Section item) {
            titleTextView.setText(item.getTitle());
        }
    }
}
