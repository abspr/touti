package ir.abspr.touti.main.main.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import ir.abspr.touti.main.main.model.wordDetails.Definition;
import ir.abspr.touti.main.main.model.wordDetails.Discussion;
import ir.abspr.touti.main.main.model.wordDetails.Movie;
import ir.abspr.touti.main.main.model.wordDetails.Music;
import ir.abspr.touti.main.main.model.wordDetails.News;

@Entity(tableName = "tbl_word")
public class Word implements Parcelable {
    @PrimaryKey
    private int id;
    private String word;
    private String phonetic;
    private Discussion discussion;
    private List<Definition> definitions;
    private List<News> news;
    private List<Movie> movies;
    private List<Music> musics;
    private boolean isPinned;

    public boolean isPinned() {
        return isPinned;
    }

    public void setPinned(boolean pinned) {
        isPinned = pinned;
    }

    public int getId() {
        return id;
    }

    public String getWord() {
        return word;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public void setPhonetic(String phonetic) {
        this.phonetic = phonetic;
    }

    public void setDiscussion(Discussion discussion) {
        this.discussion = discussion;
    }

    public void setDefinitions(List<Definition> definitions) {
        this.definitions = definitions;
    }

    public void setNews(List<News> news) {
        this.news = news;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }

    public void setMusics(List<Music> musics) {
        this.musics = musics;
    }

    public String getPhonetic() {
        return phonetic;
    }

    public Discussion getDiscussion() {
        return discussion;
    }

    public List<Definition> getDefinitions() {
        return definitions;
    }

    public List<News> getNews() {
        return news;
    }

    public List<Movie> getMovies() {
        return movies;
    }

    public List<Music> getMusics() {
        return musics;
    }


    public Word() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.word);
        dest.writeString(this.phonetic);
        dest.writeParcelable(this.discussion, flags);
        dest.writeTypedList(this.definitions);
        dest.writeTypedList(this.news);
        dest.writeTypedList(this.movies);
        dest.writeTypedList(this.musics);
        dest.writeByte(this.isPinned ? (byte) 1 : (byte) 0);
    }

    protected Word(Parcel in) {
        this.id = in.readInt();
        this.word = in.readString();
        this.phonetic = in.readString();
        this.discussion = in.readParcelable(Discussion.class.getClassLoader());
        this.definitions = in.createTypedArrayList(Definition.CREATOR);
        this.news = in.createTypedArrayList(News.CREATOR);
        this.movies = in.createTypedArrayList(Movie.CREATOR);
        this.musics = in.createTypedArrayList(Music.CREATOR);
        this.isPinned = in.readByte() != 0;
    }

    public static final Creator<Word> CREATOR = new Creator<Word>() {
        @Override
        public Word createFromParcel(Parcel source) {
            return new Word(source);
        }

        @Override
        public Word[] newArray(int size) {
            return new Word[size];
        }
    };
}
