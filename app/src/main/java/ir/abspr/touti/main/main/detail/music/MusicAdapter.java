package ir.abspr.touti.main.main.detail.music;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import co.mobiwise.library.MusicPlayerView;
import ir.abspr.touti.R;
import ir.abspr.touti.main.main.RVAdapter;
import ir.abspr.touti.main.main.RVViewHolder;
import ir.abspr.touti.main.main.model.wordDetails.Music;

public class MusicAdapter extends RVAdapter<Music, MusicAdapter.MusicViewHolder> {


    public MusicAdapter(List<Music> items) {
        super(items);
    }

    @NonNull
    @Override
    public MusicViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_music, parent, false);
        return new MusicViewHolder(view);
    }

    class MusicViewHolder extends RVViewHolder<Music> {

        TextView sentenceTextView;
        TextView translationTextView;
        MusicPlayerView musicPlayerView;

        public MusicViewHolder(@NonNull View itemView) {
            super(itemView);
            sentenceTextView = itemView.findViewById(R.id.music_tv_sentence);
            translationTextView = itemView.findViewById(R.id.music_tv_translation);
            musicPlayerView = itemView.findViewById(R.id.music_player);
        }

        @Override
        public void bind(Music item) {
            sentenceTextView.setText(item.getSentance());
            translationTextView.setText(item.getTranslation());
            musicPlayerView.setProgressVisibility(false);
            musicPlayerView.setCoverDrawable(R.drawable.artwok);
            musicPlayerView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (musicPlayerView.isRotating()) {
                        musicPlayerView.stop();
                        musicPlayerView.updateCoverRotate();
                    } else {
                        musicPlayerView.start();
                    }
                }
            });
        }
    }
}
