package ir.abspr.touti.main.main.detail.news;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import ir.abspr.touti.R;
import ir.abspr.touti.main.main.RVAdapter;
import ir.abspr.touti.main.main.RVViewHolder;
import ir.abspr.touti.main.main.model.wordDetails.News;

public class NewsAdapter extends RVAdapter<News, NewsAdapter.NewsViewHolder> {


    public NewsAdapter(List<News> items) {
        super(items);
    }

    @NonNull
    @Override
    public NewsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_news, viewGroup, false);
        return new NewsViewHolder(view);
    }

    class NewsViewHolder extends RVViewHolder<News> {

        TextView englishSentenceTextView;
        TextView translateTextView;
        TextView sourceAndDateTextView;

        public NewsViewHolder(@NonNull View itemView) {
            super(itemView);

            englishSentenceTextView = itemView.findViewById(R.id.news_tv_english);
            translateTextView = itemView.findViewById(R.id.news_tv_translate);
            sourceAndDateTextView = itemView.findViewById(R.id.news_tv_sourceAndDate);
        }

        @Override
        public void bind(News item) {
            englishSentenceTextView.setText(item.getSentance());
            translateTextView.setText(item.getTranslation());
            sourceAndDateTextView.setText(item.getSource() + "  |  " + item.getDate());
        }
    }
}
