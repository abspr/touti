package ir.abspr.touti.main.main.detail.definition;

import android.os.Bundle;
import android.view.View;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import ir.abspr.touti.R;
import ir.abspr.touti.main.main.base.BaseFragment;
import ir.abspr.touti.main.main.model.wordDetails.Definition;

public class DefinitionFragment extends BaseFragment {

    private List<Definition> definitions;

    public DefinitionFragment(List<Definition> definitions) {
        this.definitions = definitions;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_definitions;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupViews(view);
    }


    private void setupViews(View view) {
        RecyclerView recyclerView = view.findViewById(R.id.definition_rv);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        DefinitionAdapter definitionAdapter = new DefinitionAdapter(definitions);
        recyclerView.setAdapter(definitionAdapter);
    }
}
