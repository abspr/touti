package ir.abspr.touti.main.main.Utils;

import java.util.List;

public class Utils {
    public static String appendItemsWithCommas(List<String> list) {
        String str = "";
        for (int index = 0; index < list.size(); index++) {
            str = str.concat(list.get(index));
            if (!(index == list.size() - 1)) {
                str = str + " ,  ";
            }
        }
        return str;
    }
}
