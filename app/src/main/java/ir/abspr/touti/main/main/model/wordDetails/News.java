package ir.abspr.touti.main.main.model.wordDetails;

import android.os.Parcel;
import android.os.Parcelable;

public class News implements Parcelable {
    private String sentance;
    private String translation;
    private String source;
    private String date;

    public void setSentance(String sentance) {
        this.sentance = sentance;
    }

    public void setTranslation(String translation) {
        this.translation = translation;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSentance() {
        return sentance;
    }

    public String getTranslation() {
        return translation;
    }

    public String getSource() {
        return source;
    }

    public String getDate() {
        return date;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.sentance);
        dest.writeString(this.translation);
        dest.writeString(this.source);
        dest.writeString(this.date);
    }

    public News() {
    }

    protected News(Parcel in) {
        this.sentance = in.readString();
        this.translation = in.readString();
        this.source = in.readString();
        this.date = in.readString();
    }

    public static final Parcelable.Creator<News> CREATOR = new Parcelable.Creator<News>() {
        @Override
        public News createFromParcel(Parcel source) {
            return new News(source);
        }

        @Override
        public News[] newArray(int size) {
            return new News[size];
        }
    };
}
