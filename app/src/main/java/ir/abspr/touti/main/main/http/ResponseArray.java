package ir.abspr.touti.main.main.http;

import java.util.List;

public class ResponseArray<T> {

    private String status;
    private List<T> data;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }
}
