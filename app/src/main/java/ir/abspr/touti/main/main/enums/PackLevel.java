package ir.abspr.touti.main.main.enums;

import android.content.Context;

import java.util.HashMap;
import java.util.Map;

import ir.abspr.touti.R;

public enum PackLevel {
    EASY(0),
    MEDIUM(1),
    HARD(2);

    private int value;
    private static Map map = new HashMap<>();

    private PackLevel(int value) {
        this.value = value;
    }


    static {
        for (PackLevel pageType : PackLevel.values()) {
            map.put(pageType.value, pageType);
        }
    }

    public static PackLevel valueOf(int pageType) {
        return (PackLevel) map.get(pageType);
    }

    public int getValue() {
        return value;
    }

    public String description(Context context) {
        String text = "";
        switch (value) {
            case 0:
                text = context.getString(R.string.pack_level_easy);
                break;
            case 1:
                text = context.getString(R.string.pack_level_medium);
                break;
            case 2:
                text = context.getString(R.string.pack_level_hard);
                break;
        }
        return context.getString(R.string.pack_level_full, text);
    }


}
