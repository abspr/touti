package ir.abspr.touti.main.main.model.wordDetails;

import android.os.Parcel;
import android.os.Parcelable;

public class Music implements Parcelable {
    private String sentance;
    private String translation;
    private String title;
    private String singer;
    private String music;

    public void setSentance(String sentance) {
        this.sentance = sentance;
    }

    public void setTranslation(String translation) {
        this.translation = translation;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setSinger(String singer) {
        this.singer = singer;
    }

    public void setMusic(String music) {
        this.music = music;
    }

    public String getSentance() {
        return sentance;
    }

    public String getTranslation() {
        return translation;
    }

    public String getTitle() {
        return title;
    }

    public String getSinger() {
        return singer;
    }

    public String getMusic() {
        return music;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.sentance);
        dest.writeString(this.translation);
        dest.writeString(this.title);
        dest.writeString(this.singer);
        dest.writeString(this.music);
    }

    public Music() {
    }

    protected Music(Parcel in) {
        this.sentance = in.readString();
        this.translation = in.readString();
        this.title = in.readString();
        this.singer = in.readString();
        this.music = in.readString();
    }

    public static final Parcelable.Creator<Music> CREATOR = new Parcelable.Creator<Music>() {
        @Override
        public Music createFromParcel(Parcel source) {
            return new Music(source);
        }

        @Override
        public Music[] newArray(int size) {
            return new Music[size];
        }
    };
}
