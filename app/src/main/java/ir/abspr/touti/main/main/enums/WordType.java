package ir.abspr.touti.main.main.enums;

import android.content.Context;

import java.util.HashMap;
import java.util.Map;

import androidx.core.content.ContextCompat;
import ir.abspr.touti.R;
public enum WordType {
    Noun(0),
    Verb(1),
    Adjective(2),
    Adverb(3);

    private int value;
    private static Map map = new HashMap<>();

    private WordType(int value) {
        this.value = value;
    }


    static {
        for (WordType type : WordType.values()) {
            map.put(type.value, type);
        }
    }

    public static WordType valueOf(int type) {
        return (WordType) map.get(type);
    }

    public int getValue() {
        return value;
    }


    public String description(Context context) {
        String text = "";
        switch (value) {
            case 0:
                text = context.getString(R.string.word_type_noun);
                break;
            case 1:
                text = context.getString(R.string.word_type_verb);
                break;
            case 2:
                text = context.getString(R.string.word_type_adjective);
                break;
            case 3:
                text = context.getString(R.string.word_type_adverb);
                break;
        }
        return text;
    }


    public int bgColor(Context context) {
        int color = 0;
        switch (value) {
            case 0:
                color = ContextCompat.getColor(context, R.color.word_type_noun);
                break;
            case 1:
                color = ContextCompat.getColor(context, R.color.word_type_verb);
                break;
            case 2:
                color = ContextCompat.getColor(context, R.color.word_type_adjective);
                break;
            case 3:
                color = ContextCompat.getColor(context, R.color.word_type_adverb);
                break;
        }
        return color;
    }

}

