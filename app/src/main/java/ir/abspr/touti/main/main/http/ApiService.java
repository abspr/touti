package ir.abspr.touti.main.main.http;

import io.reactivex.Single;
import ir.abspr.touti.main.main.model.Pack;
import ir.abspr.touti.main.main.model.Section;
import ir.abspr.touti.main.main.model.Word;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiService {

    @GET("ymf6l")
    Single<ResponseArray<Pack>> packages();

    @GET("199v71")
    Single<ResponseArray<Section>> sections(@Query("id") int packId);

    @GET("apgl1")
    Single<ResponseArray<Word>> words();

}
