package ir.abspr.touti.main.main.word;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import ir.abspr.touti.R;
import ir.abspr.touti.main.main.RVAdapter;
import ir.abspr.touti.main.main.RVViewHolder;
import ir.abspr.touti.main.main.model.Word;

public class WordAdapter extends RVAdapter<Word, WordAdapter.WordViewHolder> {


    public WordAdapter(List<Word> items) {
        super(items);
    }

    @NonNull
    @Override
    public WordViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_word, viewGroup, false);
        return new WordViewHolder(view);
    }


    class WordViewHolder extends RVViewHolder<Word> {

        TextView wordTextView;
        TextView definitionTextView;
        ImageView pinImageView;

        public WordViewHolder(@NonNull View itemView) {
            super(itemView);
            wordTextView = itemView.findViewById(R.id.words_tv_word);
            definitionTextView = itemView.findViewById(R.id.words_tv_definition);
            pinImageView = itemView.findViewById(R.id.words_iv_reviewBtn);
        }

        @Override
        public void bind(Word item) {
            wordTextView.setText(item.getWord());
            definitionTextView.setText(item.getDefinitions().get(0).getTranslation());
        }
    }

}
