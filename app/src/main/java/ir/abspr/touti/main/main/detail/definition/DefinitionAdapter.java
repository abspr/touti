package ir.abspr.touti.main.main.detail.definition;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import ir.abspr.touti.R;
import ir.abspr.touti.main.main.RVAdapter;
import ir.abspr.touti.main.main.RVViewHolder;
import ir.abspr.touti.main.main.Utils.Utils;
import ir.abspr.touti.main.main.model.wordDetails.Definition;
import ir.abspr.touti.main.main.model.wordDetails.Example;

public class DefinitionAdapter extends RVAdapter<Definition, DefinitionAdapter.DefinitionViewHolder> {


    public DefinitionAdapter(List<Definition> items) {
        super(items);
    }

    @NonNull
    @Override
    public DefinitionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_definition, parent, false);
        return new DefinitionViewHolder(view);
    }

    class DefinitionViewHolder extends RVViewHolder<Definition> {

        private TextView typeTextView;
        private TextView definitionTextView;
        private TextView translationTextView;
        private RecyclerView exampleRecyclerView;
        private TextView synonymsTextView;

        public DefinitionViewHolder(@NonNull View itemView) {
            super(itemView);
            typeTextView = itemView.findViewById(R.id.definition_tv_type);
            definitionTextView = itemView.findViewById(R.id.definition_tv_definition);
            translationTextView = itemView.findViewById(R.id.definition_tv_translation);
            exampleRecyclerView = itemView.findViewById(R.id.definition_rv_examples);
            exampleRecyclerView.setLayoutManager(new LinearLayoutManager(itemView.getContext(), RecyclerView.VERTICAL, false));
            synonymsTextView = itemView.findViewById(R.id.definition_tv_synonyms);
        }

        @Override
        public void bind(Definition item) {
            definitionTextView.setText(item.getDefinition());
            translationTextView.setText(item.getTranslation());
            synonymsTextView.setText(Utils.appendItemsWithCommas(item.getSynonyms()));
//            typeTextView.setBackgroundResource(item.type().bgColor(itemView.getContext()));
            typeTextView.setText(item.type().description(itemView.getContext()));
            setupRecyclerView(item.getExamples());
        }

        private void setupRecyclerView(List<Example> examples) {
            exampleRecyclerView.setAdapter(new ExampleAdapter(examples));
        }
    }
}
