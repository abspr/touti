package ir.abspr.touti.main.main.adapters;

import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class PicassoImageLoading {
    public static void loadImage(ImageView imageView, String path) {
        Picasso.get().load(path).into(imageView);
    }
}
