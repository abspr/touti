package ir.abspr.touti.main.main.model.db;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import io.reactivex.Single;
import ir.abspr.touti.main.main.model.Pack;

@Dao
public interface PackDao {

    @Query("SELECT * FROM tbl_pack")
    Single<List<Pack>> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Pack> packs);

}
