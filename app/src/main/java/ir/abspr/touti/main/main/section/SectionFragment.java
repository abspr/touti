package ir.abspr.touti.main.main.section;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import io.reactivex.Scheduler;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ir.abspr.touti.R;
import ir.abspr.touti.main.main.OnRVItemClickListener;
import ir.abspr.touti.main.main.adapters.ImageLoadingService;
import ir.abspr.touti.main.main.base.ObserverFragment;
import ir.abspr.touti.main.main.http.ApiContainer;
import ir.abspr.touti.main.main.http.ResponseArray;
import ir.abspr.touti.main.main.model.Pack;
import ir.abspr.touti.main.main.model.Section;
import ir.abspr.touti.main.main.model.repo.SectionRepository;

public class SectionFragment extends ObserverFragment  {

    private SectionViewModel viewModel;
    private SectionAdapter sectionAdapter;
    private Pack pack;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pack = getArguments().getParcelable("pack");
        viewModel = new SectionViewModel(new SectionRepository(ApiContainer.getApiService()));
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fillHeader();
    }

    @Override
    public void subscribe() {
        viewModel.getSections(pack.getId())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<ResponseArray<Section>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(ResponseArray<Section> sectionResponseArray) {
                        RecyclerView recyclerView = getView().findViewById(R.id.sections_rv);
                        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2, LinearLayoutManager.VERTICAL, false));
                        sectionAdapter = new SectionAdapter(sectionResponseArray.getData());
                        sectionAdapter.setOnRvItemClickListener((item, position) -> {
                            Bundle bundle = new Bundle();
                            bundle.putParcelable("section", sectionResponseArray.getData().get(position));
                            Navigation.findNavController(getView()).navigate(R.id.action_sectionFragment_to_wordFragment, bundle);
                        });
                        recyclerView.setAdapter(sectionAdapter);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_section;
    }


    private void fillHeader() {
        TextView titleTextView = getView().findViewById(R.id.sectionsHeader_tv_title);
        TextView sectionTextView = getView().findViewById(R.id.sectionsHeader_tv_sections);
        TextView wordTextView = getView().findViewById(R.id.sectionsHeader_tv_words);
        TextView levelTextView = getView().findViewById(R.id.sectionsHeader_tv_level);
        ImageView coverImageView = getView().findViewById(R.id.sectionsHeader_iv_cover);
        ImageButton backButton = getView().findViewById(R.id.section_ivb_back);

        titleTextView.setText(pack.getTitle());
        sectionTextView.setText(getContext().getString(R.string.pack_sections_number, pack.getSections()));
        wordTextView.setText(getContext().getString(R.string.pack_words_number, pack.getWords()));
        levelTextView.setText(pack.level().description(getContext()));
        ImageLoadingService.loadImage(coverImageView, pack.getCover());
        backButton.setOnClickListener(view -> {
            Navigation.findNavController(view).popBackStack();
        });
    }
}
