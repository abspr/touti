package ir.abspr.touti.main.main;

import android.view.View;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public abstract class RVAdapter<T, E extends RVViewHolder<T>> extends RecyclerView.Adapter<E> {

    private OnRVItemClickListener<T> onItemClickListener;
    protected List<T> items;



    public RVAdapter() {
    }

    public RVAdapter(List<T> items) {
        this.items = items;
    }


    public void setItems(List<T> items) {
        this.items = items;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public void onBindViewHolder(@NonNull E holder, int position) {
        holder.bind(items.get(position));

        if (onItemClickListener != null) {
            holder.itemView.setOnClickListener( v -> {
                onItemClickListener.onItemClick(items.get(position), position);
            });
        }
    }

    public void setOnRvItemClickListener(OnRVItemClickListener<T> onRvItemClickListener) {
        this.onItemClickListener = onRvItemClickListener;
    }
}


