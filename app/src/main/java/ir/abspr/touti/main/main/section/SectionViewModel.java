package ir.abspr.touti.main.main.section;

import io.reactivex.Single;
import ir.abspr.touti.main.main.http.ResponseArray;
import ir.abspr.touti.main.main.model.Section;
import ir.abspr.touti.main.main.model.repo.SectionRepository;

public class SectionViewModel {

    private SectionRepository sectionRepository;


    public SectionViewModel(SectionRepository sectionRepository) {
        this.sectionRepository = sectionRepository;
    }


    public Single<ResponseArray<Section>> getSections(int packId) {
        return sectionRepository.sections(packId);
    }
}
