package ir.abspr.touti.main.main.model.repo;

import io.reactivex.Single;
import ir.abspr.touti.main.main.http.ApiService;
import ir.abspr.touti.main.main.http.ResponseArray;
import ir.abspr.touti.main.main.model.Word;

public class WordRepository {
    private ApiService cloudDataSource;

    public WordRepository(ApiService cloudDataSource) {
        this.cloudDataSource = cloudDataSource;
    }

    public Single<ResponseArray<Word>> words() {
        return cloudDataSource.words();
    }
}
