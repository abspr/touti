package ir.abspr.touti.main.main.model.wordDetails;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

import ir.abspr.touti.main.main.enums.WordType;

public class Definition implements Parcelable {
    private int type;
    private String definition;
    private String translation;
    private List<String> synonyms;
    private List<Example> examples;

    public void setType(int type) {
        this.type = type;
    }

    public void setDefinition(String definition) {
        this.definition = definition;
    }

    public void setTranslation(String translation) {
        this.translation = translation;
    }

    public void setSynonyms(List<String> synonyms) {
        this.synonyms = synonyms;
    }

    public void setExamples(List<Example> examples) {
        this.examples = examples;
    }

    public String getDefinition() {
        return definition;
    }

    public String getTranslation() {
        return translation;
    }

    public List<String> getSynonyms() {
        return synonyms;
    }

    public List<Example> getExamples() {
        return examples;
    }

    public WordType type() {
        return WordType.valueOf(type);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.type);
        dest.writeString(this.definition);
        dest.writeString(this.translation);
        dest.writeStringList(this.synonyms);
        dest.writeList(this.examples);
    }

    public Definition() {
    }

    protected Definition(Parcel in) {
        this.type = in.readInt();
        this.definition = in.readString();
        this.translation = in.readString();
        this.synonyms = in.createStringArrayList();
        this.examples = new ArrayList<Example>();
        in.readList(this.examples, Music.class.getClassLoader());
    }

    public static final Parcelable.Creator<Definition> CREATOR = new Parcelable.Creator<Definition>() {
        @Override
        public Definition createFromParcel(Parcel source) {
            return new Definition(source);
        }

        @Override
        public Definition[] newArray(int size) {
            return new Definition[size];
        }
    };
}
