package ir.abspr.touti.main.main.model.wordDetails;

import android.os.Parcel;
import android.os.Parcelable;

public class Movie implements Parcelable {
    private String sentance;
    private String translation;
    private String title;
    private String cover;
    private String movie;

    public void setSentance(String sentance) {
        this.sentance = sentance;
    }

    public void setTranslation(String translation) {
        this.translation = translation;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public void setMovie(String movie) {
        this.movie = movie;
    }

    public String getSentance() {
        return sentance;
    }

    public String getTranslation() {
        return translation;
    }

    public String getTitle() {
        return title;
    }

    public String getCover() {
        return cover;
    }

    public String getMovie() {
        return movie;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.sentance);
        dest.writeString(this.translation);
        dest.writeString(this.title);
        dest.writeString(this.cover);
        dest.writeString(this.movie);
    }

    public Movie() {
    }

    protected Movie(Parcel in) {
        this.sentance = in.readString();
        this.translation = in.readString();
        this.title = in.readString();
        this.cover = in.readString();
        this.movie = in.readString();
    }

    public static final Parcelable.Creator<Movie> CREATOR = new Parcelable.Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel source) {
            return new Movie(source);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };
}
