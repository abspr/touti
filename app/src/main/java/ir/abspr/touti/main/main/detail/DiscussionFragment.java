package ir.abspr.touti.main.main.detail;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import ir.abspr.touti.R;
import ir.abspr.touti.main.main.base.BaseFragment;
import ir.abspr.touti.main.main.model.wordDetails.Discussion;


public class DiscussionFragment extends BaseFragment {


    private Discussion discussion;

    public DiscussionFragment(Discussion discussion) {
        this.discussion = discussion;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_discussion;
    }



    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fill();
    }

    private void fill() {
        TextView mainTextView = getView().findViewById(R.id.discussion_tv_main);
        TextView subTextView = getView().findViewById(R.id.discussion_tv_sub);

        mainTextView.setText(discussion.getMain());
        subTextView.setText(discussion.getSub());
    }
}
