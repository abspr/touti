package ir.abspr.touti.main.main.home;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import io.reactivex.Scheduler;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ir.abspr.touti.R;
import ir.abspr.touti.main.main.base.ObserverFragment;
import ir.abspr.touti.main.main.http.ApiContainer;
import ir.abspr.touti.main.main.http.ResponseArray;
import ir.abspr.touti.main.main.model.Pack;
import ir.abspr.touti.main.main.model.repo.PackRepository;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class HomeFragment extends ObserverFragment {

    private HomeViewModel viewModel;

    private PackAdapter packAdapter;
    private ReviewAdapter reviewAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        viewModel = new HomeViewModel(new PackRepository(ApiContainer.getApiService()));

    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        RecyclerView reviewRecyclerView = view.findViewById(R.id.main_rv_review);
        reviewRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, true));
        reviewAdapter = new ReviewAdapter();
        reviewRecyclerView.setAdapter(reviewAdapter);
    }

    @Override
    public void subscribe() {
        viewModel.getPacks()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<ResponseArray<Pack>>() {

                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(ResponseArray<Pack> packResponseArray) {
                        RecyclerView packRecyclerView = getView().findViewById(R.id.main_rv_packs);
                        packRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                        packAdapter = new PackAdapter(packResponseArray.getData(), getContext());
                        packAdapter.setOnRvItemClickListener(((item, position) -> {
                            Bundle bundle = new Bundle();
                            bundle.putParcelable("pack", packResponseArray.getData().get(position));
                            Navigation.findNavController(getView()).navigate(R.id.action_homeFragment_to_sectionFragment, bundle);
                        }));
                        packRecyclerView.setAdapter(packAdapter);
                    }

                    @Override
                    public void onError(Throwable e) {
                    }
                });
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_home;
    }
}
