package ir.abspr.touti.main.main.word;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ir.abspr.touti.R;
import ir.abspr.touti.main.main.base.ObserverFragment;
import ir.abspr.touti.main.main.http.ApiContainer;
import ir.abspr.touti.main.main.http.ResponseArray;
import ir.abspr.touti.main.main.model.Section;
import ir.abspr.touti.main.main.model.Word;
import ir.abspr.touti.main.main.model.repo.WordRepository;

public class WordFragment extends ObserverFragment {

    private WordViewModel viewModel;
    private WordAdapter wordAdapter;

    private Section section;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new WordViewModel(new WordRepository(ApiContainer.getApiService()));
        section = getArguments().getParcelable("section");
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews();
    }

    @Override
    public void subscribe() {
        viewModel.getWords()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<ResponseArray<Word>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(ResponseArray<Word> wordResponseArray) {
                        RecyclerView recyclerView = getView().findViewById(R.id.words_rv);
                        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                        wordAdapter = new WordAdapter(wordResponseArray.getData());
                        wordAdapter.setOnRvItemClickListener((item, position) -> {
                            Bundle bundle = new Bundle();
                            bundle.putParcelable("word", wordResponseArray.getData().get(position));
                            Navigation.findNavController(getView()).navigate(R.id.action_wordFragment_to_detailFragment, bundle);
                        });
                        recyclerView.setAdapter(wordAdapter);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", "onError: ", e);
                    }
                });
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_word;
    }


    private void setupViews() {

        TextView titleTextView = getView().findViewById(R.id.wordHeader_tv_title);
        TextView sectionsTextView = getView().findViewById(R.id.wordHeader_tv_sections);
        ImageButton backButton = getView().findViewById(R.id.word_ivb_back);

        titleTextView.setText(section.getTitle());

        backButton.setOnClickListener(view -> {
            Navigation.findNavController(view).popBackStack();
        });
    }
}
