package ir.abspr.touti.main.main.detail;

import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.Navigation;
import androidx.viewpager.widget.ViewPager;
import ir.abspr.touti.R;
import ir.abspr.touti.main.main.base.ObserverFragment;
import ir.abspr.touti.main.main.model.Word;

public class DetailFragment extends ObserverFragment {


    private Word word;
    private TabLayout tabLayout;
    private ViewPager viewPager;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        word = getArguments().getParcelable("word");
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fillHeader();
        tabLayout = view.findViewById(R.id.detail_tabLayout);
        viewPager = view.findViewById(R.id.detail_viewPager);
        viewPager.setAdapter(new DetailViewPagerAdapter(getFragmentManager(), word));
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public void subscribe() {

    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_detail;
    }


    private void fillHeader() {
        TextView wordTextView = getView().findViewById(R.id.detail_tv_word);
        TextView phoneticTextView = getView().findViewById(R.id.detail_tv_phonetic);
        ImageButton backButton = getView().findViewById(R.id.detail_ivb_back);

        wordTextView.setText(word.getWord());
        phoneticTextView.setText(word.getPhonetic());

        backButton.setOnClickListener(view -> {
            Navigation.findNavController(view).popBackStack();
        });
    }
}
