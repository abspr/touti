package ir.abspr.touti.main.main.model.wordDetails;

import android.os.Parcel;
import android.os.Parcelable;

public class Discussion implements Parcelable {
    private String main;
    private String sub;

    public void setMain(String main) {
        this.main = main;
    }

    public void setSub(String sub) {
        this.sub = sub;
    }

    public String getMain() {
        return main;
    }

    public String getSub() {
        return sub;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.main);
        dest.writeString(this.sub);
    }

    public Discussion() {
    }

    protected Discussion(Parcel in) {
        this.main = in.readString();
        this.sub = in.readString();
    }

    public static final Parcelable.Creator<Discussion> CREATOR = new Parcelable.Creator<Discussion>() {
        @Override
        public Discussion createFromParcel(Parcel source) {
            return new Discussion(source);
        }

        @Override
        public Discussion[] newArray(int size) {
            return new Discussion[size];
        }
    };
}
