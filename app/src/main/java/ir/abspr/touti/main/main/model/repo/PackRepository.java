package ir.abspr.touti.main.main.model.repo;

import java.util.List;

import io.reactivex.Single;
import ir.abspr.touti.main.main.http.ApiContainer;
import ir.abspr.touti.main.main.http.ApiService;
import ir.abspr.touti.main.main.http.ResponseArray;
import ir.abspr.touti.main.main.model.Pack;
import ir.abspr.touti.main.main.model.db.PackDao;

public class PackRepository {

    private ApiService cloudDataSource;
    private PackDao localDataSource;


    public PackRepository(ApiService cloudDataSource) {
        this.cloudDataSource = cloudDataSource;
    }

    public Single<ResponseArray<Pack>> packages() {
        return cloudDataSource.packages();
    }

}
