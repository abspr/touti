package ir.abspr.touti.main.main.model.wordDetails;

import android.os.Parcel;
import android.os.Parcelable;

public class Example implements Parcelable {
    private String sentance;
    private String translation;

    public void setSentance(String sentance) {
        this.sentance = sentance;
    }

    public void setTranslation(String translation) {
        this.translation = translation;
    }

    public String getSentance() {
        return sentance;
    }

    public String getTranslation() {
        return translation;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.sentance);
        dest.writeString(this.translation);
    }

    public Example() {
    }

    protected Example(Parcel in) {
        this.sentance = in.readString();
        this.translation = in.readString();
    }

    public static final Parcelable.Creator<Example> CREATOR = new Parcelable.Creator<Example>() {
        @Override
        public Example createFromParcel(Parcel source) {
            return new Example(source);
        }

        @Override
        public Example[] newArray(int size) {
            return new Example[size];
        }
    };
}
