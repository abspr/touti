package ir.abspr.touti.main.main;

public interface OnRVItemClickListener<T> {

    void onItemClick(T item, int position);

}
