package ir.abspr.touti.main.main.word;

import io.reactivex.Single;
import ir.abspr.touti.main.main.http.ResponseArray;
import ir.abspr.touti.main.main.model.Word;
import ir.abspr.touti.main.main.model.repo.WordRepository;

public class WordViewModel {

    private WordRepository wordRepository;

    public WordViewModel(WordRepository wordRepository) {
        this.wordRepository = wordRepository;
    }


    Single<ResponseArray<Word>> getWords() {
        return wordRepository.words();
    }
}
