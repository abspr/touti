package ir.abspr.touti.main.main.detail.movie;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import ir.abspr.touti.R;
import ir.abspr.touti.main.main.RVAdapter;
import ir.abspr.touti.main.main.RVViewHolder;
import ir.abspr.touti.main.main.base.BaseFragment;
import ir.abspr.touti.main.main.model.wordDetails.Movie;

public class MovieAdapter extends RVAdapter<Movie, MovieAdapter.MovieViewHolder> {


    public MovieAdapter(List<Movie> items) {
        super(items);
    }

    @NonNull
    @Override
    public MovieViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_movie, parent, false);
        return new MovieViewHolder(view);
    }

    class MovieViewHolder extends RVViewHolder<Movie> {

        TextView sentenceTextView;
        TextView translationTextView;
        TextView titleTextView;

        public MovieViewHolder(@NonNull View itemView) {
            super(itemView);
            sentenceTextView = itemView.findViewById(R.id.movie_tv_sentence);
            translationTextView = itemView.findViewById(R.id.movie_tv_translation);
            titleTextView = itemView.findViewById(R.id.movie_tv_title);
        }

        @Override
        public void bind(Movie item) {
            sentenceTextView.setText(item.getSentance());
            translationTextView.setText(item.getTranslation());
            titleTextView.setText(item.getTitle());
        }
    }


}
