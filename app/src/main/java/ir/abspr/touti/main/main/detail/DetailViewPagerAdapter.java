package ir.abspr.touti.main.main.detail;


import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import ir.abspr.touti.main.main.detail.definition.DefinitionFragment;
import ir.abspr.touti.main.main.detail.movie.MovieFragment;
import ir.abspr.touti.main.main.detail.music.MusicFragment;
import ir.abspr.touti.main.main.detail.news.NewsFragment;
import ir.abspr.touti.main.main.model.Word;

public class DetailViewPagerAdapter extends FragmentPagerAdapter {

    private Word word;

    public DetailViewPagerAdapter(FragmentManager fm, Word word) {
        super(fm);
        this.word = word;
    }

    @Override
    public Fragment getItem(int i) {
        switch (i) {
            case 0:
                return new DiscussionFragment(word.getDiscussion());
            case 1:
                return new DefinitionFragment(word.getDefinitions());
            case 2:
                return new NewsFragment(word.getNews());
            case 3:
                return new MovieFragment(word.getMovies());
            case 4:
                return new MusicFragment(word.getMusics());
        }
        return null;
    }

    @Override
    public int getCount() {
        return 5;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        ArrayList<String> list = new ArrayList<>();
        list.add("توضیحات");
        list.add("معانی");
        list.add("اخبار");
        list.add("فیلم");
        list.add("آهنگ");
        return list.get(position);
    }
}
