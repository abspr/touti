package ir.abspr.touti.main.main.home;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import ir.abspr.touti.R;

public class ReviewAdapter extends RecyclerView.Adapter<ReviewAdapter.ReviewViewHolder> {


    @NonNull
    @Override
    public ReviewViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_review, viewGroup, false);
        return new ReviewViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ReviewViewHolder reviewViewHolder, int i) {

    }

    @Override
    public int getItemCount() {
        return 6;
    }

    class ReviewViewHolder extends RecyclerView.ViewHolder {

        TextView wordTextView;
        TextView phoneticTextView;
        TextView definitionTextView;

        public ReviewViewHolder(@NonNull View itemView) {
            super(itemView);
            wordTextView = itemView.findViewById(R.id.review_tv_word);
            phoneticTextView = itemView.findViewById(R.id.review_tv_phonetic);
            definitionTextView = itemView.findViewById(R.id.review_tv_definition);
        }
    }

}
